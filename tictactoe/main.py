import random

class TicTacToe:
    def __init__(self):
        self.tablero = []

    def crear_tablero(self):
        for i in range(3):
            row = []
            for j in range(3):
                row.append('-')
            self.tablero.append(row)
        
    
    def mostrar_tablero(self):
        for row in self.tablero:
            for item in row:
                print(item, end=" ")
            print()


    def get_random_primer_jugador(self):
        return random.randint(0, 1)
    
    def fijar_posicion(self, row, col, jugador):
        self.tablero[row][col] = jugador

    def fijar_tablero(self):
        for row in self.tablero:
            for item in row:
                if item == '-':
                    return False
        return True
    
    def cambiar_jugador(self, jugador):
        if jugador == 'O': 
            return 'X' 
        else: 
            return 'O'

    def ganar_jugador(self, jugador):
        win = None
        n = len(self.tablero)

        # chequeando horizontal
        for i in range(n):
              win = True
              for j in range(n):
                   if self.tablero[i][j] != jugador:
                    win = False
                    break
              if win:
                    return win
        # chequeando vertical
        for i in range(n):
            win = True
            for j in range(n):
                if self.tablero[j][i] != jugador:
                    win = False
                    break
            if win:
                    return win

        # chequeando diagonales
        win = True
        for i in range(n):
            if self.tablero[i][i] != jugador:
                        win = False
                        break
        if win:
            return win

         
        win = True
        for i in range(n):
            if self.tablero[i][n - 1 - i] != jugador:
                win = False
                break
        if win:
                return win
        return False

        self.fijar_tablero()

    

    def empezar(self):
        self.crear_tablero()

        if self.get_random_primer_jugador() == 1: 
            jugador = 'X' 
        else:
            jugador = 'O'
        
        while True:
            try:
               print("juega el jugador ", jugador)
               
               # el usuario mueve ficha
               row, col = list(map(int, input("Introduzca 2 numeros para la posicion (fila y columna por separado): ").split()))
               while (row > 3) or (col > 3) or (row == 0) or (col == 0):
                  print("\n ¡¡¡ Te has salido del tablero, Introduzca numeros 1-3")
                  row, col = list(map(int, input("Introduzca 2 numeros para la posicion (fila y columna por separado): ").split()))
                # fijar la posicion
               self.fijar_posicion(row - 1, col - 1, jugador)
               self.mostrar_tablero()
            except:
               print("\n ¡¡¡ ERROOOOOOOR !!!, has perdido tu turno")
               

            # chequear si el jugador gana el juego
            if self.ganar_jugador(jugador):
                print("el jugador",jugador, "gana el juego!")
                break
  
            # chequeando si el juego esta dibujado
            if self.fijar_tablero():
                print("dibujar partida!")
                break

            # cambiar de turno
            jugador = self.cambiar_jugador(jugador)

            # mostrar el trablero al final del juego
            #print()
            #self.mostrar_tablero()



# Empezar el juego
tic_tac_toe = TicTacToe()
tic_tac_toe.empezar()




     

